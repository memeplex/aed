% Octavo Set
% Carlos Pita
% 14 de Junio de 2016

# Consideraciones generales

Se entregan los ejercicios 4 y 5, en la modalidad "full" del enunciado. En todos
los casos, se empleó un optimizador por descenso de gradientes con parámetros
$\alpha=5 \cdot 10^{-4}$, $\epsilon=5 \cdot 10^{-4}$ y sin cota superior al
número de iteraciones, donde $\alpha$ es la tasa de aprendizaje y $\epsilon$ es
la máxima distancia euclídea tolerada entre iteraciones sucesivas para el vector
de coeficientes. El modelo estimado en cada caso fue un modelo logístico; es
decir, un modelo lineal generalizado con:

1. Una función logística como *link* inverso.
2. Una función de pérdida basada en (menos el logaritmo de) la función de
   verosimilitud (de un modelo binomial).

Los cálculos de cada *update* se efectuaron en forma matricial para, al menos,
reducir la constante de un algoritmo de lenta convergencia en términos de O
grande.

Para todos los ajustes se evaluaron tres alternativas de normalización: sin
normalizar, normalizando por media y desvío estándar y normalizando por mínimo y
máximo. Todos los ajustes se evaluaron estimando mediante validación cruzada de
5 *folds* el valor esperado para las siguientes funciones: el error de
predicción cuadrático, la pérdida logarítmica y el *f-score*. En general, los
puntajes no favorecieron de forma decisiva a ninguna de las normalizaciones, si
bien el *f-score* tendió a ser consistentemente mayor en los ajustes sin
normalizar, que además resultan más fácilmente interpretables en términos de la
consigna original.

Al final de este informe se presenta la salida de pantalla para cada modelo y
normalización considerados.

# Ejercicio 4

Se estimaron los modelos $E(y_i) = l(\beta_0 + \beta_1 x_i)$ y $E(y_i) =
l(\beta_0 + \beta_1 x_i + \beta_2 x^2_i)$, siendo $l$ la función logística.
Conociendo el proceso generador, es evidente que el segundo modelo no puede
capturar ningún efecto sistemático adicional, lo cual se refleja en las
estimaciones: las de los coeficientes $\beta_0$ y $\beta_1$ son esencialmente
las mismas en ambos ajustes, mientras que la del coeficiente $\beta_2$ es
virtualmente cero.

## Ejercicio 4.e

Para este proceso generador, incluir un término cuadrático en el modelo tiene
más sentido. Se evaluaron los mismos modelos que para el punto anterior y,
adicionalmente, el modelo de grado 3 $E(y_i) = l(\beta_0 + \beta_1 x_i +
\beta_2 x^2_i + \beta_3 x^3_i)$, que no debería aportar información
significativa. En este caso, el modelo de grado 2 captura el efecto cuadrático.
No obstante, sin normalizar los datos, los errores esperados estimados aumentan
ligeramente respecto al modelo más simple. Hay dos razones, relacionadas entre
sí: (i) el efecto cuadrático es muy débil en relación al efecto lineal (ya que
$2 |z| > 0.2 z^2$ para $z$ en $(-1, 1)$) y (ii) el término cuadrático también es
débil en relación al término lineal (ya que $|z|
> z^2$ para $z$ en $(-1, 1)$). La normalización atenúa el problema (ii) y los
errores esperados para el modelo de segundo grado efectivamente disminuyen. El
modelo de tercer grado suele rankear peor que los demás y no aporta información
relevante.

# Ejercicio 5

Este experimento es semejante a tratar de predecir si dos monedas justas cayeron
o bien ambas del mismo lado, o bien sobre lados diferentes, teniendo la
oportunidad de observar solo una de ellas: el lado del que cae la moneda no
observada no es afectado por el lado sobre el que cae la moneda observada y, por
lo tanto, ambas predicciones posibles tienen la misma probabilidad. La razón por
la que la analogía funciona, a pesar de que el modelo pedido incluye "ambas
monedas", es que las incluye de forma aditiva, de tal forma que cada término
solo puede "observar una de las monedas". La figura \ref{fig:ej5} ilustra lo
dicho: sobre cualquier horizontal o sobre cualquier vertical, la proporción
(esperada) de azules es la misma que la proporción (esperada) de rojos.
Como cabe esperar, todos los coeficientes estimados para el modelo se encuentran
muy próximos al cero.

\break

![Clasificación en grupos cero (rojo) y uno (azul).\label{fig:ej5}](ej5.png)

\break

# Salidas de pantalla

## Sin normalizar

```
4a, 4b, 4c, 4d --------------------------

$s1 sqloss    logloss    fscore 
    0.1418521 0.4315612  0.7821577 

$b1 0.09755988  1.37018283

$s2 sqloss    logloss    fscore 
    0.1419670 0.4319935  0.7809917 

$b2 0.08528683  1.37097202 -0.01168855

4e --------------------------------------

$s1 sqloss    logloss    fscore 
    0.1261945 0.3933699  0.8067227 

$b1 1.128179  1.633143

$s2 sqloss    logloss    fscore 
    0.1263204 0.3939549  0.8046709 

$b2 1.14950280  1.57084972  0.04777519

$s3 sqloss   logloss   fscore 
    0.126426 0.393717  0.805526 

$b3 1.05845463  1.49295937 -0.10426750  0.07901354

5 ---------------------------------------

$s sqloss    logloss    fscore 
   0.2504672 0.6941336  0.6846986 

$b 0.07167850  0.09066444 -0.11472890
```

\break

## Normalizando por media y desvío

```
4a, 4b, 4c, 4d --------------------------

$s1 sqloss    logloss    fscore 
    0.1303729 0.4075760  0.8054968 

$b1 0.1358659  2.3747958

$s2 sqloss    logloss    fscore 
    0.1304312 0.4073793  0.8029819 

$b2 0.03956234  2.38078714  0.19917665

4e --------------------------------------

$s1 sqloss    logloss    fscore 
    0.1347299 0.4198106  0.7932844 

$b1 0.128461  2.233774

$s2 sqloss    logloss   fscore 
    0.1345731 0.4178063 0.7879440 

$b2 .1018447 1.9977517 0.5446813

$s3 sqloss    logloss   fscore 
    0.1346583 0.4182156 0.7908992 

$b3 .1857971 1.7933588 0.4047697 0.6839886

5 ---------------------------------------

$s sqloss    logloss   fscore 
   0.2516587 0.6964720 0.4563319 

$b 0.007726371 -0.051085244 -0.061841561
```

\break

## Normalizando por mínimo y máximo

```
4a, 4b, 4c, 4d --------------------------

$s1 sqloss    logloss   fscore 
    0.1301091 0.4064709 0.8074922 

$b1 5.958045 11.669027

$s2 sqloss    logloss   fscore 
    0.1302258 0.4070147 0.8087774 

$b2 5.9741827 11.6662421  0.3074584

4e --------------------------------------

$s1 sqloss    logloss   fscore 
    0.1346356 0.4133211 0.7894180 

$b1 5.958216 12.320203

$s2 sqloss    logloss   fscore 
    0.1346406 0.4131260 0.7837259 

$b2 5.546295 11.083747  3.539124

$s3 sqloss    logloss   fscore 
    0.1346167 0.4126588 0.7832618 

$b3 5.878339 10.857182  3.358532  1.818766

5 ---------------------------------------

$s sqloss    logloss   fscore 
   0.2492607 0.6916241 0.5284339 

$b 0.3513042 -0.4643355 -0.1445022
```
