

# Clasificador dummy, ignora todo y devuelve 1 (superviviente) o 0 (fallecido) al azar.
# INPUT:
# - dataX: datos de entrada para el clasificador (vector con pclass, name, sex, age, sib, parch, ticket, fare, cabin, embarked)
# OUTPUT:
# - 0 si no sobrevivio, 1 si lo ha hecho.
dummyClassifier <- function(dataX){
  return(sample(0:1, 1, replace=F)[1])
}


# Validador.
# INPUT:
# - dataX: matriz con los datos X, cada fila es un dato (pclass, name, sex, age, sib, parch, ticket, fare, cabin, embarked), tal cual como viene en el archivo.
# - survived: vector con 0 (no sobrevivio) y 1 (sobrevivio), tantos como filas tenga dataX.
# - clasificador: clasificador que se usara, tomar como base de codificacion el dummyClassifier.
# OUTPUT:
# Lista con los parametros para armar una matriz de confusion (TP, TN, FP, FN) y un indice de aciertos (rate).
validador <- function(dataX, survived, clasificador){
  stopifnot(nrow(dataX)==length(survived))
  TP <- 0
  TN <- 0
  FP <- 0
  FN <- 0
  for (i in 1:length(survived)){
    clasePredicha <- clasificador(dataX[i,])
    claseReal <- survived[i]
    if (clasePredicha == 0 && claseReal == 0){
      TN <- TN + 1
    } else if (clasePredicha == 0 && claseReal == 1){
      FN <- FN + 1
    } else if (clasePredicha == 1 && claseReal == 0){
      FP <- FP + 1
    } else if (clasePredicha == 1 && claseReal == 1){
      TP <- TP + 1
    } else {
      stop("NO DEBERIA LLEGAR AQUI!")
    }
  }
  total <- TN + FN + FP + TP
  rate <- (TP+TN)/total
  precision <- TP / (TP + FP)
  recall <- TP / (TP + FN)
  Fscore <- 2*precision*recall / (precision + recall)
  res <- list(FP = FP, FN = FN, TP = TP, TN=TN, rate=rate, precision=precision, recall = recall, Fscore = Fscore)
  return(res)
}



prueba <- function(){
  dataY <- sample(0:1, 150, replace=TRUE)
  dataX <- matrix(rep(0,150), nrow=150)
  res <- validador(dataX, dataY, dummyClassifier)
}






