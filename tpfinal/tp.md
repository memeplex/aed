% Trabajo Final
% Carlos Pita y Pablo Roccatagliata
% 18 de Julio de 2016

---
toc: yes
numbersections: yes
---

# Introducción

Reconociendo que no contamos con un modelo estructural para el problema, ni con
el tiempo o la formación necesarios para llevar a cabo una investigación
socioeconómica, histórica, geográfica, etc. que podría arrojar luz sobre
estructuras poco evidentes del problema, preferimos limitarnos a extraer
"manualmente" la estructura más superficial de los datos y ofrecer al sistema de
aprendizaje un modelo relativamente flexible y rico, en base al que pueda
seleccionar un submodelo óptimo sobre una trayectoria de soluciones indexada
por parámetros de complejidad y evaluada mediante técnicas de remuestreo.

# Criterios generales

## Tratamiento de datos faltantes

En lugar de imputar medidas de tendencia tomadas sobre los datos
existentes^[Hicimos algunas pruebas en esta dirección, pero no conseguimos
mejorar visiblemente el score estimado de nuestro estimador respecto al enfoque
adoptado.], consideramos a los datos faltantes como una posible fuente de
información, *i.e.* la razón por la que faltan puede ser sistemática en algún
caso. En el caso de variables naturalmente categóricas, este tratamiento
consiste simplemente en agregar una categoría `NONE` al conjunto de valores que
puede tomar la variable. Para las variables numéricas, en cambio, los valores
faltantes se reemplazan por 0 y se introduce una nueva variable binaria
`<var>None` (donde `<var>` es el nombre de la variable original) que vale 1 si,
y solo si, la variable original presentaba un valor faltante; la interacción de
estas dos variables es siempre 0 y solamente una de ellas puede aparecer
"prendida" en una fila de la matriz de diseño. Será el estimador el que decida
si la falta de información revela o no alguna regularidad en los datos.

## Tratamiento de variables categóricas

Las variables categóricas se representaron como *factors* de R que,
eventualmente, aparecen en la matriz de diseño como variables binarias. Como no
contamos con el total de los datos, no podemos asegurar que el conjunto de
valores al que tenemos acceso para cada variable sea el conjunto completo. Por
ello, introdujimos la categoría auxiliar `ANOTHER`, a la cual ---durante la
etapa de preprocesamiento--- van a para todos los valores no reconocidos para
una variable determinada. Como esta categoría, por definición, nunca aparece
durante el aprendizaje, es neutra respecto a la predicción, *i.e.* consiste en
un mero expediente técnico para anular los valores desconocidos.

## Selección de modelos

Como se dijo, preferimos armar un modelo flexible y rico, y dejar que el sistema
de aprendizaje decida sobre la relevancia de cada variable y término del modelo.
Evaluamos dos tecnologías de aprendizaje: *random forests* y modelo lineal
generalizado logístico (con regularización por *lasso* y *ridge*), implementadas
por los paquetes `randomForest` y `glmnet` de R, respectivamente. Como
conseguimos mejores resultados con la regresión logística, describimos en esta
subsección las características del modelo lineal que terminamos adoptando. Dicho
modelo incluye todas las variables que se detallan en la sección final, junto a
sus interacciones de a pares^[Con excepción de algunas interacciones
incompatibles, como `<var>` con `<varNone>`.] y ---para completar el polinomio
de grado 2--- los términos cuadráticos para las variables numéricas. La
selección del submodelo óptimo consistió en recorrer, por aproximaciones
sucesivas, una jerarquía de grillas de parámetros que controlan la intensidad de
la regularización y la composición de la misma en términos de *lasso* y *ridge*.
Aunque el modelo completo contiene ---una vez expandidas las variables
categóricas y sus interacciones--- más de 1000 términos, todos los submodelos
competitivos contienen alrededor de 100 términos.

# Tratamiento especial de variables

Algunas variables presentan una estructura superficial evidente que nosotros
podemos "parsear" más directa y eficientemente que el sistema de aprendizaje.
A continuación, describimos el tratamiento de los casos especiales más
relevantes.

## Tickets

La variable `Ticket` se descompuso en:

- `TicketNum`: la parte numérica del ticket. Si el ticket no fue reportado, o
  no contiene una parte numérica, `TicketNum` adopta el valor 0 y la variable
  binaria auxiliar `TicketNumNone` vale 1.
- `TicketPref`: el prefijo no numérico que, en ocasiones, antecede a la parte
  numérica, separada por un espacio en blanco entre ambos. Si el ticket no fue
  reportado o no contiene un prefijo, `TicketPref` adopta la categoría `None`.

## Nombres

De la variable `Name` extrajimos la variable categórica `Title`, que incluye los
valores `MR`, `MISS`, `MRS` y `ANOTHER`. Si el nombre no incluye ningún título
reconocido, el valor de `Title` será `ANOTHER`.

Experimentamos también con métricas más sofisticadas del tamaño familiar,
obtenidas a partir de agrupar los apellidos comunes por puerto de embarque y
clase, pero no conseguimos mejoras sustanciales respecto a las métricas
provistas por el *data set* original, que miden el tamaño de las familias de
forma horizontal (`SibSp`) y vertical (`Parch`).

## Cabinas

El tratamiento de la variable `Cabin` es más complejo. Algunas características
de esta variable son:

- Algunas entradas no contienen cabinas y otras contienen más de una.
- Por lo general, una cabina se codifica mediante una letra inmediatamente
  seguida de un número, pero en ocasiones la letra aparece suelta.
- Por lo general, cuando aparece más de una cabina, la letra es común a todas y
  los números son muy próximos entre sí.

Teniendo en cuenta estas observaciones, cada entrada se descompuso en:

- `CabinCount`: el número de cabinas listadas. Si este número es 0, la
  variable binaria auxiliar `CabinCountNone` será 1, ya que interpretamos la
  ausencia de cabinas como un dato faltante y no como un valor con sentido
  numérico estricto.
- `CabinLetter`: la letra de la primera cabina listada, o `NONE` si no se
  listan cabinas.
- `CabinNum`: el número de la primera cabina listada, o 0 si no se listan
  cabinas o la cabina considerada consiste únicamente en una letra,
  en cuyo caso la variable binaria auxiliar `CabinNumNone` será 1.

# Modelo completo

El modelo completo en base al que se seleccionó el submodelo óptimo, expresado
según la gramática de fórmulas de R, es:

```
# Término fijo
1 + (
# Términos simples e interacciones de a pares entre ellos
    Pclass +
    SibSp +
    Parch +
    Fare + FareNone +
    TicketPref +
    TicketNum + TicketNumNone +
    Title +
    Sex +
    Embarked +
    CabinCount + CabinCountNone +
    CabinLetter +
    CabinNum + CabinNumNone +
    Age + AgeNone
) ^ 2 + (
# Términos cuadráticos para las variables numéricas
    I(SibSp^2) +
    I(Parch^2) +
    I(Fare^2) +
    I(TicketNum^2) +
    I(CabinCount^2) +
    I(CabinNum^2) +
    I(Age^2)
) - (
# Interacciones espurias que siempre son nulas
    Age : AgeNone +
    Fare : FareNone +
    TicketNum : TicketNumNone +
    CabinCount : CabinCountNone +
    CabinNum : CabinNumNone
)
```

Cabe señalar que los términos de esta expresión no corresponden directamente a
las columnas de la matriz de diseño, ya que las variables categóricas y sus
interacciones se expanden a variables binarias. Como se comentó, la matriz de
diseño contiene más de 1000 columnas, por lo que no resulta práctico listarlas
aquí.

# Resultados

El modelo resultante se obtuvo tras un proceso de exploración de submodelos,
sobre un subespacio de soluciones indexado por dos parámetros:

- $\lambda$: la fuerza total de la regularización.

- $\alpha$: la composición de la regularización en términos de *lasso* y *ridge*
  (concretamente, la combinación lineal convexa de ambos, *i.e. elastic net*).

Fijado un valor para $\alpha$, el paquete `glmnet` puede construir muy
eficientemente una trayectoria de soluciones para una secuencia de valores de
$\lambda$, evaluando cada solución mediante validación cruzada. Para explorar
valores de $\alpha$, en cambio, procedimos por aproximación sucesiva, comenzando
con una exploración grosera en el rango 0-1, continuando con una exploración más
fina en la mitad más prometedora del rango y así sucesivamente, a fin de
reducir logarítmicamente el orden de complejidad que requeriría una exploración
fina sobre el rango total.

Como ya se comentó, los modelos competitivos resultantes de este proceso
contienen aproximadamente el $10\%$ de los términos del modelo original,
indicando que la regularización requerida es muy fuerte. Esto no es
sorprendente, dado nuestro *approach* de enriquecer el modelo y depender de la
regularización para la selección de términos, y considerando que partimos de
una matriz de diseño "ancha" ($p \approx 1000 > n \approx 700$).

El modelo seleccionado (indexado por $\lambda = 0.0106$ y $\alpha = 0.85$)
presenta un porcentaje de aciertos (calculado por validación cruzada 10-*fold*)
de $83,7\%$, que parece bastante competitivo a juzgar por los comentarios del
[tutorial](https://www.kaggle.com/c/titanic/details/getting-started-with-random-forests)
publicado en Kaggle:

> Know that a score of 0.79 - 0.81 is doing well on this challenge, and
> 0.81-0.82 is really going beyond the basic models! [...] NOTE: You may see
> some people on the Leaderboard show accuracy of .90 up to even 100% -- but
> that's probably not from statistical modeling,
