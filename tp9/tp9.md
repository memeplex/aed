% Noveno Set
% Carlos Pita
% 24 de Junio de 2016

# Ejercicio 5

Se implementó un clasificador por $k$ vecinos más cercanos con las siguientes
características:

1. Si dos vecinos se encuentran a la misma distancia y no pueden entrar ambos al
   conjunto de vecinos más cercanos, se decide al azar cuál de ellos entra.
2. Si como resultado de la votación se observa un empate entre dos o más clases,
   la votación se desempata al azar.
3. Para calcular la distancia se emplea (el cuadrado de) la norma euclídea.

Los datos de entrada se normalizaron centrando cada $feature$ en su media y
escalándola por su desvío estándar.

Para seleccionar el $k$ óptimo se utilizó un esquema de validación cruzada con 5
$folds$ sobre el $70\%$ de los datos, tomando como costo una función de pérdida
0-1. La figura \ref{fig:k} muestra la tasa de error de clasificación estimada en
función de cada $k$ considerado.

![Tasa de error de clasificación en función de $k$.\label{fig:k}](k.png)

Se validó el estimador construido en base al $k$ óptimo (6) contra el
$30\%$ de los datos inicialmente apartados a este fin. La matriz de confusión
resultante se presenta a continuación:

```
                     Predicho
    Observado         Iris-setosa Iris-versicolor Iris-virginica
      Iris-setosa              15               0              0
      Iris-versicolor           0              13              0
      Iris-virginica            0               1             16
```

Como se puede observar, la tasa de error es menor al $5\%$.
