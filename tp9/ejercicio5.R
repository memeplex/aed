knn <- function(x, X, y, k) {
    n <- length(y)
    dists <- apply(X, 1, function(x.) sum((x - x.)^2))
    votes <- table(y[order(dists, runif(n))[1:min(k, n)]])
    ties <- which(votes == max(votes))
    ties[[sample(length(ties), 1)]]
}

cv <- function(folds, X, y, k) {
    sum(sapply(1:length(folds), function(i) {
        test <- folds[[i]]
        train <- do.call(c, folds[-i])
        pred <- apply(X[test,], 1, function(x) knn(x, X[train,], y[train], k))
        sum(pred != as.numeric(y[test]))
    })) / length(y)
}

cm <- function(X., y., X, y, k) {
    l <- levels(y)
    pred <- apply(X., 1, function(x) knn(x, X, y, k))
    table(data.frame(Observado=y., Predicho=factor(l[pred], l)))
}

norm <- function(x) (x - mean(x)) / sd(x)

set.seed(1)
iris <- read.csv('iris.data')
n <- nrow(iris)
iris <- iris[sample(1:n),]  # shuffle
for (i in 1:4) iris[i] <- norm(iris[[i]])  # normalize
X <- as.matrix(iris[,1:4])
y <- factor(iris[[5]])

k <- 1:30
m <- round(n * 0.7)
folds <- split(sample(1:m), cut(1:m, 5))
loss <- sapply(k, function(k) cv(folds, X[1:m,], y[1:m], k))
best.k <- k[which.min(loss)]
jpeg('k.png', width=1024, height=480)
plot(k, loss, type='b')
dev.off()

print(cm(X[(m+1):n,], y[(m+1):n], X[1:m,], y[1:m], best.k))
