data <- read.csv('Credit.csv', header=T)
n <- nrow(data)
y <- data$Balance
test.i <- sample(n, n/3, replace=F)

fit <- function(X, y) {
    solve(t(X) %*% X) %*% (t(X) %*% y)
}

mse <- function(b, X, y) {
    mean((y - X %*% b)^2)
}

test <- function(name, X, y) {
    b <- fit(X[-test.i,], y[-test.i])
    e <- mse(b, X[test.i,], y[test.i])
    cat(name, ': ', b, ' // ', e, '\n', sep='')
}

################################################################################
cat('\npunto a:\n')
X <- cbind(1, data$Income, data$Limit, data$Rating, data$Student == 'Yes')
cat('1+Income+Limit+Rating+Student:', fit(X, y), '\n')

################################################################################
cat('\npunto b:\n')
X <- cbind(X, data$Ethnicity == 'Asian', data$Ethnicity == 'Caucasian')
cat('1+Income+Limit+Rating+Student+Asian+Caucasian:', fit(X, y), '\n')

################################################################################
cat('\npunto c:\n')
test('1+Income+Rating+Student',
     cbind(1, data$Income, data$Rating, data$Student == 'Yes'), y)
test('1+Income+Student', cbind(1, data$Income, data$Student == 'Yes'), y)
test('1+Rating+Student', cbind(1, data$Rating, data$Student == 'Yes'), y)
test('1+Income+Rating', cbind(1, data$Income, data$Rating), y)
test('1+Income', X <- cbind(1, data$Income), y)
test('1+Rating', X <- cbind(1, data$Rating), y)
test('1+Student', X <- cbind(1, data$Student == 'Yes'), y)
test('1', X <- cbind(rep(1, n)), y)
