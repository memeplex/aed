% Séptimo Set
% Carlos Pita
% 2 de Junio de 2016

# Ejercicio 5

Para cada una de las estandarizaciones $x$, $(x-\mu)/\sigma$ y
$(x-min(x))/(max(x)-min(x))$ se ejecutaron 400 corridas del algoritmo de
descenso de gradientes, una por cada par $(\alpha, \epsilon)$ ---donde $\alpha$
es la tasa de aprendizaje y $\epsilon$ la máxima distancia cuadrática aceptada
entre sucesivos vectores $(\beta_0, \beta_1, \beta_2)$--- de una grilla que
contiene combinaciones de $\alpha$ moviéndose entre $5 \cdot 10^{-5}$ y $5 \cdot
10^{-4}$ y $\epsilon$ moviéndose en el mismo rango \footnote{Por encima de estos
rangos el algoritmo incurre a menudo en divergencias o errores numéricos.}. El
vector de coeficientes inicial se fijo en $(0, 0, 0)$ para todas las corridas.

Las figuras \ref{fig:nok} y \ref{fig:nol} muestran, respectivamente, diagramas
de contorno para la cantidad de iteraciones y la distancia cuadrática al vector
de coeficientes estimado por mínimos cuadrados, para cada par $(\alpha,
\epsilon)$ de la grilla. En base a las figuras resulta claro que conviene tomar
$\alpha$ grande y $\epsilon$ no demasiado grande para alcanzar un buen
compromiso entre las dos métricas (número de iteraciones vs. distancia al
"verdadero" valor). El mínimo número de iteraciones es de 118, con un error de
0.0108; el mínimo error es de 0.0011, con una cantidad de iteraciones de 170.

Similarmente, las figuras \ref{fig:mdk} y \ref{fig:mdl} presentan las
superficies de contorno obtenidas al estandarizar por media y desvío estándar,
mientras que las figuras \ref{fig:mmk} y \ref{fig:mml} aquellas obtenidas al
estandarizar por mínimo y máximo. Teniendo en cuenta todas las corridas, los
mejores resultados se obtuvieron al normalizar por media y desvío estándar: un
error mínimo de 0.0004 con 81 iteraciones, y un número de iteraciones mínimo de
59 con error de 0.004. Los números promedio de ambas métricas también son muy
favorables a esta estandarización. El detalle se consigna en la salida de
pantalla al final del informe.

El balance óptimo entre $\alpha$ y $\epsilon$ dependerá de la importancia
relativa que tengan para una aplicación en cuestión el número de iteraciones
(que afecta la cantidad de recursos empleados, tanto computacionales como el
tiempo de espera) y la precisión del resultado (que afecta la toma de decisiones
basada en la estimación).

![Número de interaciones, sin estandarizar.\label{fig:nok}](nostd-k.png)

![Distancia al "verdadero" valor, sin estandarizar.\label{fig:nol}](nostd-l.png)

![Número de interaciones, estandarizando por media y desvío.\label{fig:mdk}](mdstd-k.png)

![Distancia al "verdadero" valor, estandarizando por media y desvío.\label{fig:mdl}](mdstd-l.png)

![Número de interaciones, estandarizando por mínimo y máximo.\label{fig:mmk}](mmstd-k.png)

![Distancia al "verdadero" valor, estandarizando por mínimo y máximo.\label{fig:mml}](mmstd-l.png)

\break

```
Corrida sin estandarizar:
  Iteraciones:
    Promedio: 291.4675
    Minimo: 118 en alpha = 5e-04 y epsilon = 5e-04 con error = 0.01076483 
  Distancia al "verdadero" beta:
    Promedio: 0.01698118
    Minimo: 0.001091183 en alpha = 5e-04 y epsilon = 5e-05 con iteraciones = 170 
  Coeficientes estimados por minimos cuadrados: 1.984459 0.4985303 0.002389031 
  Coeficientes estimados por descenso de gradiente: 1.983393 0.4985263 0.002621264 

Corrida estandarizando por media y desvio:
  Iteraciones:
    Promedio: 149.185
    Minimo: 59 en alpha = 5e-04 y epsilon = 0.0004763158 con error = 0.004177445 
  Distancia al "verdadero" beta:
    Promedio: 0.007119825
    Minimo: 0.0004191173 en alpha = 5e-04 y epsilon = 5e-05 con iteraciones = 81 
  Coeficientes estimados por minimos cuadrados: 1.990526 0.497618 0.005685876 
  Coeficientes estimados por descenso de gradiente: 1.990134 0.4974878 0.00576029 

Corrida estandarizando por minimo y maximo:
  Iteraciones:
    Promedio: 772.465
    Minimo: 311 en alpha = 7.368421e-05 y epsilon = 5e-04 con error = 0.5595724 
  Distancia al "verdadero" beta:
    Promedio: 0.1239268
    Minimo: 0.008534558 en alpha = 5e-04 y epsilon = 5e-05 con iteraciones = 756 
  Coeficientes estimados por minimos cuadrados: 1.988751 0.4913711 0.02081034 
  Coeficientes estimados por descenso de gradiente: 1.984176 0.4973869 0.02477504
```
