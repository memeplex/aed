% Décimo Set
% Carlos Pita
% 12 de Julio de 2016

# Ejercicio 4

Se implementó un clasificador por Análisis Discriminante Lineal (Bayesiano).
Algunos supuestos fuertes de este tipo de clasificador son:

1. Los predictores se distribuyen normalmente condicionales a la clase.
2. Dichas distribuciones condicionales son, además, homoscedásticas.

Como las distribuciones condicionales de los predictores son multivariadas no es
fácil "verificar visualmente" estos supuestos. Sin embargo, en la figura
\ref{fig:hists} se observa que, en general, la proyección a cada predictor no
viola de manera evidente los supuestos, excepto para uno o dos casos
(especialmente la cuarta variable condicional a la clase `Iris-setosa`). Si bien
estas proyecciones no confirman que los supuestos son adecuados, al menos no los
contradicen de plano.

![Histogramas para cada variable condicional a cada clase.\label{fig:hists}](hists.png)

\break

El clasificador se entrenó usando el $70\%$ (muestreado aleatoriamente)
de los datos y se validó construyendo la matriz de confusión que se lista debajo
sobre el $30\%$ restante de los datos:

```
                                       Predicho
    Observado         Iris-setosa Iris-versicolor Iris-virginica
      Iris-setosa              15               0              0
      Iris-versicolor           0              14              0
      Iris-virginica            0               0             16
```

Como se puede observar, la clasificación resultó perfecta sobre este $30\%$ de
los datos.
